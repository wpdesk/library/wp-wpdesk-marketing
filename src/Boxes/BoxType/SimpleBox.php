<?php
/**
 * Simple box.
 *
 * @package WPDesk\Library\Marketing\Abstracts
 */

namespace WPDesk\Library\Marketing\Boxes\BoxType;

class SimpleBox extends Box {

	const TYPE = 'simple';
}
