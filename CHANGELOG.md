## [1.1.3] - 2023-12-28
### Fixed
- Added escaping functions before output.

## [1.1.2] - 2022-05-23
### Fixed
- fatal
- bbcodes

## [1.1.1] - 2022-05-12
### Fixed
- url target
- img bbcode

## [1.1.0] - 2021-10-12
### Fixed
- library update

## [1.0.0] - 2021-09-16
### Added
- init
