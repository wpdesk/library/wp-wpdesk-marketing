<?php

namespace unit;

use PHPUnit\Framework\MockObject\MockObject;
use WP_Mock;
use WP_Mock\Tools\TestCase;
use WPDesk\Library\Marketing\Boxes\BoxType\ImageBox;
use WPDesk\Library\Marketing\Boxes\BoxType\SimpleBox;
use WPDesk\Library\Marketing\Boxes\BoxType\SliderBox;
use WPDesk\Library\Marketing\Boxes\BoxType\VideoBox;
use WPDesk\View\Renderer\Renderer;


class BoxTest extends TestCase {
	/**
	 * @var MockObject|ObjectDataInterface
	 */
	private $object_data;

	/**
	 * @var MockObject|Renderer
	 */
	private $renderer;
	/**
	 * @var MockObject|ObjectDataInterface
	 */
	private $cache_manager_under_test;

	public function setUp(): void {
		WP_Mock::setUp();
		$this->renderer = $this->getMockBuilder( Renderer::class )->setMethods( [
			'set_resolver',
			'render',
			'output_render',
		] )->getMock();
	}

	public function tearDown(): void {
		WP_Mock::tearDown();
	}

	public function testSchouldCheckBoxInstanceOf() {
		// Except.
		$expected = 'WPDesk\Library\Marketing\Boxes\Abstracts\BoxInterface';

		// When.
		$actual = new ImageBox( [], $this->renderer );

		// Then.
		$this->assertInstanceOf( $expected, $actual );
	}

	public function testSchouldCheckObjectData() {
		// When.
		$actual = new SimpleBox(
			[
				'type'        => 'simple',
				'slug'        => 'slug',
				'links'       => [],
				'title'       => 'Box title',
				'description' => 'Description',
				'className'   => 'myclass',
				'close_row'   => [ 'yes' ],
				'open_row'    => [ 'no' ],
			],
			$this->renderer
		);

		// Then.
		$this->assertSame( 'simple', $actual->get_type() );
		$this->assertSame( 'slug', $actual->get_slug() );
		$this->assertSame( [], $actual->get_links() );
		$this->assertSame( 'Box title', $actual->get_title() );
		$this->assertSame( 'Description', $actual->get_description() );
		$this->assertSame( 'myclass', $actual->get_class() );
		$this->assertTrue( $actual->get_row_close() );
		$this->assertFalse( $actual->get_row_open() );
	}

	public function testShouldCheckBoxTypes() {
		$ImageBox = new ImageBox( [], $this->renderer );
		$this->assertSame( 'image', $ImageBox->get_type() );

		$VideoBox = new VideoBox( [], $this->renderer );
		$this->assertSame( 'video', $VideoBox->get_type() );

		$SliderBox = new SliderBox( [], $this->renderer );
		$this->assertSame( 'slider', $SliderBox->get_type() );

		$SimpleBox = new SimpleBox( [], $this->renderer );
		$this->assertSame( 'simple', $SimpleBox->get_type() );
	}

}
