<?php
/**
 * PHPUnit bootstrap file
 */

require_once dirname( __DIR__, 2 ) . '/vendor/autoload.php';

WP_Mock::setUsePatchwork( true );
WP_Mock::bootstrap();
