<?php

namespace unit;

use WP_Mock;
use WP_Mock\Tools\TestCase;
use WPDesk\Library\Marketing\Boxes\Helpers\Markers;

class MarkersTest extends TestCase {

	public function setUp(): void {
		WP_Mock::setUp();
	}

	public function tearDown(): void {
		WP_Mock::tearDown();
	}

	public function testSchouldCheckMarkerAppend() {
		// Except.
		WP_Mock::userFunction( 'get_site_url' )->once()->andReturn( 'http://site.pl/' );
		WP_Mock::userFunction( 'current_time' )->once()->andReturn( '2021-09-08' );
		$expected = new Markers();

		// When.
		$expected->add_placeholder( '{date}', current_time( 'mysql' ) );

		// Then.
		$this->assertArrayHasKey( '{date}', $expected->get_placeholders() );
	}

}
