<?php

namespace unit;

use WP_Mock;
use WP_Mock\Tools\TestCase;
use WPDesk\Library\Marketing\Boxes\Helpers\BBCodes;

class BBCodesTest extends TestCase {

	public function setUp(): void {
		WP_Mock::setUp();
	}

	public function tearDown(): void {
		WP_Mock::tearDown();
	}

	public function testSchouldCheckBBCodeAppend() {
		// Except.
		$expected = new BBCodes();

		// When.
		$expected->add_bbcode( '~\[b\](.*?)\[/b\]~s', '<strike>$1</strike>' );

		// Then.
		$this->assertArrayHasKey( '~\[b\](.*?)\[/b\]~s', $expected->get_bbcodes() );
	}

}
